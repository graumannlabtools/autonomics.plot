% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/defaults.R
\name{make_gg_colors}
\alias{make_gg_colors}
\title{Create default ggplot colors for factor levels}
\usage{
make_gg_colors(factor_levels)
}
\arguments{
\item{factor_levels}{character vector}
}
\value{
color vector (character)
}
\description{
Create default ggplot colors for factor levels
}
\seealso{
\href{https://stackoverflow.com/questions/8197559/emulate-ggplot2-default-color-palette}{stackoverflow}
}
\author{
John Colby
}
