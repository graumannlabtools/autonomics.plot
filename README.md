[![Project Status: Wip - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/0.1.0/wip.svg)](http://www.repostatus.org/#wip)

# autonomics.plot

Plot omics data.
Part of [autonomics](https://bitbucket.org/account/user/graumannlabtools/projects/autonomics), the R suite for automated omics data analysis.

# Installation

To install the package, you first need the
[*devtools*](https://github.com/hadley/devtools) package.

```{r}
install.packages("devtools")
```

Then you can install the *autonomics.plot* package using

```{r}
library(devtools)
install_bitbucket("graumannlabtools/autonomics.plot")
```

# Functionality

`plot_sample_distributions`    
`plot_feature_profiles`    
`plot_feature_distributions`    
`plot_feature_boxes`
`plot_feature_heatmap`
